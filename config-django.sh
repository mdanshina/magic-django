#!/bin/bash

DIR=$(dirname $(readlink -f $0))

sudo /bin/bash -c "
	mkdir -p /etc/service/django
	cp ${DIR}/service/run /etc/service/django
	chmod +x /etc/service/django/run
	
	sv restart django > /dev/null
"
